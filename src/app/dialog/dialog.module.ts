import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DialogRoutingModule } from './dialog-routing.module';
import { HomeDialogComponent } from './home-dialog/home-dialog.component';
import { SectionTitleComponent } from '../shared/section-title/section-title.component';
import { ModalComponent } from './modal/modal.component';
import { ComponentTitleComponent } from '../shared/component-title/component-title.component';
import { AccordionComponent } from './accordion/accordion.component';
import { IconsModule } from '../shared/icons/icons.module';

@NgModule({
  declarations: [HomeDialogComponent, ModalComponent, AccordionComponent],
  imports: [
    CommonModule,
    DialogRoutingModule,
    SectionTitleComponent,
    ComponentTitleComponent,
    IconsModule,
  ],
})
export class DialogModule {}
