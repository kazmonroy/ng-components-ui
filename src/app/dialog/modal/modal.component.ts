import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  inject,
} from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit, OnDestroy {
  private el = inject(ElementRef);
  @Output() modalClose = new EventEmitter<boolean>();

  @Input() action = '';
  ngOnInit(): void {}

  close() {
    this.modalClose.emit(false);
  }

  ngOnDestroy(): void {
    this.el.nativeElement.remove();
  }
}
