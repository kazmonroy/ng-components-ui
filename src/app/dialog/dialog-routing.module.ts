import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeDialogComponent } from './home-dialog/home-dialog.component';

const routes: Routes = [{ path: '', component: HomeDialogComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DialogRoutingModule {}
