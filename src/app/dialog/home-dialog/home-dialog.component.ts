import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-home-dialog',
  templateUrl: './home-dialog.component.html',
  styleUrls: ['./home-dialog.component.scss'],
})
export class HomeDialogComponent {
  modalOpen = false;

  questions = [
    {
      q: 'What is your refund policy?',
      a: 'Lorem ipsum dolor sit amet consectetur.',
    },
    {
      q: 'Do you offer technical support?',
      a: 'Yes.',
    },
  ];

  toggleModal() {
    this.modalOpen = !this.modalOpen;
  }
}
