import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-accordion',
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.scss'],
})
export class AccordionComponent {
  @Input() FAQS!: { q: string; a: string }[];
  openItemIndex = 0;

  toggle(index: number) {
    index === this.openItemIndex
      ? (this.openItemIndex = -1)
      : (this.openItemIndex = index);
  }
}
