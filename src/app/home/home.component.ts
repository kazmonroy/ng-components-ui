import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent {
  thumbs = [
    {
      svg: 'accordion',
      path: '/dialog',
    },
    {
      svg: 'list',
      path: '/views',
    },
    {
      svg: 'modal',
      path: '/dialog',
    },
    { svg: 'placeholder', path: '/elements' },
    { svg: 'segments', path: '/elements' },
    { svg: 'stats', path: '/views' },
    { svg: 'table', path: '/collections' },
    { svg: 'tabs', path: '/collections' },
  ];
}
