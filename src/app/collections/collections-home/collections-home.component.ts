import { Component } from '@angular/core';

@Component({
  selector: 'app-collections-home',
  templateUrl: './collections-home.component.html',
  styleUrls: ['./collections-home.component.scss'],
})
export class CollectionsHomeComponent {
  data = [
    {
      name: 'Kaz',
      age: 32,
      job: 'Designer',
    },
    {
      name: 'Alice',
      age: 30,
      job: 'Dev',
    },
    {
      name: 'Emmie',
      age: 34,
      job: 'Engineer',
    },
  ];

  headers = [
    {
      key: 'name',
      label: 'Name',
    },
    {
      key: 'age',
      label: 'Age',
    },
    {
      key: 'job',
      label: 'Job',
    },
  ];
}
