import { Component, Input } from '@angular/core';

export interface Data {
  name: string;
  age: number;
  job: string;
}

export interface Headers {
  key: string;
  label: string;
}
@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent {
  @Input() data!: any;
  @Input() headers!: Headers[];
  @Input() tableStyle = '';
}
