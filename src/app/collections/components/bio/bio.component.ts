import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-bio',
  templateUrl: './bio.component.html',
  styleUrls: ['./bio.component.scss'],
})
export class BioComponent {
  items = [
    {
      imgUrl: './assets/bg-1.jpg',
      title: 'Gradient 1',
      description: 'A cool gradient',
    },
    {
      imgUrl: './assets/bg-2.jpg',
      title: 'Gradient 2',
      description: 'Another cool gradient',
    },
  ];
}
