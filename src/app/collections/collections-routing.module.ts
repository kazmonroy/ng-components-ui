import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CollectionsHomeComponent } from './collections-home/collections-home.component';
import { BioComponent } from './components/bio/bio.component';
import { PartnersComponent } from './components/partners/partners.component';
import { ContactComponent } from './components/contact/contact.component';

const routes: Routes = [
  {
    path: '',
    component: CollectionsHomeComponent,
    title: 'Collections | UI',

    children: [
      {
        path: '',
        component: BioComponent,
        title: 'Collections | UI',
      },
      {
        path: 'partners',
        component: PartnersComponent,
        title: 'Tabs - Partners | UI',
      },
      {
        path: 'contact',
        component: ContactComponent,
        title: 'Tabs - Contact | UI',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CollectionsRoutingModule {}
