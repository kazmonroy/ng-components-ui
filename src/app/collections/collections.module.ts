import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CollectionsRoutingModule } from './collections-routing.module';
import { CollectionsHomeComponent } from './collections-home/collections-home.component';
import { TableComponent } from './components/table/table.component';
import { ComponentTitleComponent } from '../shared/component-title/component-title.component';
import { BioComponent } from './components/bio/bio.component';
import { PartnersComponent } from './components/partners/partners.component';
import { ContactComponent } from './components/contact/contact.component';
import { RouterModule } from '@angular/router';
import { TabsComponent } from './components/tabs/tabs.component';
import { SectionTitleComponent } from '../shared/section-title/section-title.component';
import { ViewsModule } from '../views/views.module';

@NgModule({
  declarations: [
    CollectionsHomeComponent,
    TableComponent,
    BioComponent,
    PartnersComponent,
    ContactComponent,
    TabsComponent,
  ],
  imports: [
    CommonModule,
    CollectionsRoutingModule,
    ComponentTitleComponent,
    RouterModule,
    SectionTitleComponent,
    ViewsModule,
  ],
  exports: [],
})
export class CollectionsModule {}
