import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-list-items',
  templateUrl: './list-items.component.html',
  styleUrls: ['./list-items.component.scss'],
})
export class ListItemsComponent {
  @Input() data: { imgUrl: string; title: string; description: string }[] = [];
  @Input() image = true;
}
