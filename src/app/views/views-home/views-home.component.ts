import { Component } from '@angular/core';

@Component({
  selector: 'app-views-home',
  templateUrl: './views-home.component.html',
  styleUrls: ['./views-home.component.scss'],
})
export class ViewsHomeComponent {
  stats = [
    {
      value: 30,
      label: 'Users',
    },
    {
      value: 900,
      label: 'Revenue',
    },
    { value: 50, label: 'Reviews' },
  ];

  items = [
    {
      imgUrl: './assets/bg-1.jpg',
      title: 'Title 1',
      description: 'Lorem impsum',
    },
    {
      imgUrl: './assets/bg-2.jpg',
      title: 'Title 2',
      description: 'Lorem impsum',
    },
  ];
}
