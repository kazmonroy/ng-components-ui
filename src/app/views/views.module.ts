import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewsRoutingModule } from './views-routing.module';
import { StatisticsComponent } from './statistics/statistics.component';
import { ViewsHomeComponent } from './views-home/views-home.component';
import { ComponentTitleComponent } from '../shared/component-title/component-title.component';
import { ListItemsComponent } from './list-items/list-items.component';
import { SectionTitleComponent } from '../shared/section-title/section-title.component';

@NgModule({
  declarations: [StatisticsComponent, ViewsHomeComponent, ListItemsComponent],
  imports: [
    CommonModule,
    ViewsRoutingModule,
    ComponentTitleComponent,
    SectionTitleComponent,
  ],
  exports: [ListItemsComponent],
})
export class ViewsModule {}
