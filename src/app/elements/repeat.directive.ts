import {
  Directive,
  TemplateRef,
  ViewContainerRef,
  Input,
  inject,
} from '@angular/core';

@Directive({
  selector: '[appRepeat]',
})
export class RepeatDirective {
  private viewContainer = inject(ViewContainerRef);
  private ref = inject(TemplateRef<any>);

  constructor() {}

  @Input('appRepeat') set render(times: number) {
    this.viewContainer.clear();
    for (let i = 0; i < times; i++) {
      this.viewContainer.createEmbeddedView(this.ref, {});
    }
  }
}
