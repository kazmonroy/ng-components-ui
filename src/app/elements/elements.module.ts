import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ElementsRoutingModule } from './elements-routing.module';
import { ElementsHomeComponent } from './elements-home/elements-home.component';
import { PlaceholderComponent } from './placeholder/placeholder.component';
import { RepeatDirective } from './repeat.directive';
import { ComponentTitleComponent } from '../shared/component-title/component-title.component';
import { SegmentComponent } from './segment/segment.component';
import { SectionTitleComponent } from '../shared/section-title/section-title.component';

@NgModule({
  declarations: [
    ElementsHomeComponent,
    PlaceholderComponent,
    RepeatDirective,
    SegmentComponent,
  ],
  imports: [
    CommonModule,
    ElementsRoutingModule,
    ComponentTitleComponent,
    SectionTitleComponent,
  ],
  exports: [],
})
export class ElementsModule {}
