import { NgModule } from '@angular/core';

import { FeatherModule } from 'angular-feather';
import {
  Camera,
  Heart,
  Github,
  ChevronDown,
  ChevronUp,
  Menu,
  Gitlab,
} from 'angular-feather/icons';

const icons = {
  Camera,
  Heart,
  Github,
  ChevronDown,
  ChevronUp,
  Menu,
  Gitlab,
};

@NgModule({
  imports: [FeatherModule.pick(icons)],
  exports: [FeatherModule],
})
export class IconsModule {}
