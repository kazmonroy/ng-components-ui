import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-component-title',
  templateUrl: './component-title.component.html',
  styleUrls: ['./component-title.component.scss'],
  standalone: true,
  imports: [CommonModule],
})
export class ComponentTitleComponent {
  @Input() section = '';
}
